<?php
//start session
session_start();
 
//including the database connection file
include_once('Crud.php');
 
$crud = new Crud();
 
if(isset($_POST['add'])) {    
    $title = $crud->escape_string($_POST['title']);
 
    //insert data to database
    $sql = "INSERT INTO task (id,title) VALUES (null,'$title')";
 
    if($crud->execute($sql)){
        $_SESSION['message'] = 'task added successfully';
    }
    else{
        $_SESSION['message'] = 'Cannot add task';
    }
 
    header('location: index.php');
}
else{
    $_SESSION['message'] = 'Fill up add form first';
    header('location: index.php');
}
?>