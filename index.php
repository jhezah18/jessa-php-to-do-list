<?php
	//start session
	session_start();
 
	//crud with database connection
	include_once('Crud.php');
 
	$crud = new Crud();
 
	//fetch data
	$sql = "SELECT * FROM task";
	$result = $crud->read($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP CRUD TO-DO-LIST</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>

	<body style="background-repeat: no-repeat;background-size: cover;background-image: url('https://bit.ly/3gZZp0t');">
<div class="container">
	<h1 class="page-header text-center">PHP CRUD TO-DO-LIST</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php
				if(isset($_SESSION['message'])){
					?>
						<div class="alert alert-info text-center">
							<?php echo $_SESSION['message']; ?>
						</div>
					<?php
 
					unset($_SESSION['message']);
				}
 
			?>
		<a href="#add" id="addnews" data-toggle="modal" class="btn btn-primary">AddNew</a>
		<a href="./trash.php" data-toggle="modal"  id="trashs" class="btn btn-primary"> <i class="glyphicon glyphicon-trash"></i>Trash</a>

			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Task</th>
						<th>Created_at</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($result as $key => $row) {
							?>
							<tr>
								<td> <?php echo $row['id']; ?></td>
								<td><input type="checkbox" class="sample form-check-input">&nbsp;<?php echo $row['title']; ?></td>
								<td><?php echo $row['created_at']; ?></td>
								<td>
								<a href="#edit<?php echo $row['id']; ?>" data-toggle="modal" class="btn btn-success">Edit</a> | 
								<a href="#delete<?php echo $row['id']; ?>" data-toggle="modal" class="btn btn-danger">Delete</a>
								</td>
								<?php include('action_modal.php'); ?>
							</tr>
							<?php     
					    }
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php include('add_modal.php'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js" integrity="sha512-n/4gHW3atM3QqRcbCn6ewmpxcLAHGaDjpEBu4xZd47N0W2oQ+6q7oc3PXstrJYXcbNU1OHdQ1T7pAP+gi5Yu8g==" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" rel="stylesheet"></script>
<script>
$('.sample').change(function(){
  if (this.checked){
    $(this).parent().css("text-decoration","line-through");
  }else{
    $(this).parent().css("text-decoration","none");
  }
})
</script>
</body>

</html>